
// SVRDlg.cpp : 实现文件
//
#include "stdafx.h"

#include "SVR.h"
#include "SVRDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CSVRDlg 对话框

CSVRDlg::CSVRDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSVRDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSVRDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSVRDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_RECORD,OnRecord)
	ON_BN_CLICKED(IDC_BUT_V2T, &CSVRDlg::OnBnClickedButV2t)
	ON_BN_CLICKED(IDC_BUT_T2V, &CSVRDlg::OnBnClickedButT2v)
	ON_BN_CLICKED(IDC_BUT_INIT, &CSVRDlg::OnBnClickedButInit)
END_MESSAGE_MAP()


// CSVRDlg 消息处理程序

BOOL CSVRDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	CoInitialize(NULL);
	

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CSVRDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSVRDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CSVRDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CSVRDlg::OnBnClickedButV2t()
{
	// TODO: 在此添加控件通知处理程序代码
	HRESULT hr;
	hr=m_pSREngine.CoCreateInstance ( CLSID_SpInprocRecognizer );
	if(FAILED(hr))
	{
		MessageBox(L"m_pSREngine.CoCreateInstance");
	}
	hr = m_pSREngine->CreateRecoContext ( &m_pSRContext );//建立上下文
	if(FAILED(hr))
	{
		MessageBox(L" m_pSREngine->CreateRecoContext ");
	}

	//这里是设置事件
	HWND hwnd = GetSafeHwnd();
	hr = m_pSRContext->SetNotifyWindowMessage(hwnd,WM_RECORD,0,0);
	if(FAILED(hr)){
		MessageBox(L"SetNotifyWindowMessage");
	}

	hr=m_pSRContext->SetInterest(SPFEI(SPEI_RECOGNITION),SPFEI(SPEI_RECOGNITION));


	//这里是设置默认的音频输入
	hr = SpCreateDefaultObjectFromCategoryId(SPCAT_AUDIOIN, &m_pAudio);
	m_pSREngine->SetInput(m_pAudio,true);


	//这里是加载默认的语法规则
	ullGrammerID = 1000;
	hr=m_pSRContext->CreateGrammar(ullGrammerID,&m_pSRGrammar);
	if(FAILED(hr)){
		MessageBox(L"CreateGrammar");
	}

	WCHAR wszXMLFile[20]=L"";
	MultiByteToWideChar(CP_ACP, 0,(LPCSTR)"cmd.xml" , -1, wszXMLFile, 256);  //这里修改XML的目录
	hr=m_pSRGrammar->LoadCmdFromFile(wszXMLFile,SPLO_DYNAMIC);
	if(FAILED(hr)){
		MessageBox(L"LoadCmdFromFile");
	}
	//开启语音识别
	m_pSRGrammar->SetRuleState( NULL,NULL,SPRS_ACTIVE );
	hr =m_pSREngine->SetRecoState(SPRST_ACTIVE);
	if(FAILED(hr)){
		MessageBox(L"SetRecoState");
	}
	
}


void CSVRDlg::OnBnClickedButT2v()
{

	LPCTSTR speakContent = _T("你好,我是王松英，你是谁啊");
	// TODO: 在此添加控件通知处理程序代码
	speech(speakContent);
}


void CSVRDlg::OnBnClickedButInit()
{
	// TODO: 在此添加控件通知处理程序代码
	
}

void CSVRDlg::speech(LPCTSTR str)
{
	ISpVoice *pVoice = NULL;

	//初始化COM接口

	
	//获取SpVoice接口

	HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void**)&pVoice);


	if (SUCCEEDED(hr))
	{
		pVoice->SetVolume((USHORT)100);				//设置音量，范围是 0 -100
		pVoice->SetRate(2);							//设置速度，范围是 -10 - 10
		hr = pVoice->Speak(str, 0, NULL);

		pVoice->Release();

		pVoice = NULL;
	}

	//释放com资源
	::CoUninitialize();
}


LRESULT CSVRDlg::OnRecord(WPARAM wParam,LPARAM lParam){
	USES_CONVERSION; 
	CSpEvent event; 

	if(m_pSRContext)
	{
		while(event.GetFrom(m_pSRContext)==S_OK){

			switch (event.eEventId) 
			{ 
			case SPEI_RECOGNITION: 
				{ 
					//识别出了语音输入

					static const WCHAR wszUnrecognized[] = L"<Unrecognized>"; 

					CSpDynamicString dstrText; 

					////取得识别结果 
					if (FAILED(event.RecoResult()->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE, TRUE ,&dstrText, NULL))) 
					{ 
						dstrText = wszUnrecognized; 
					} 

					BSTR SRout; 
					dstrText.CopyToBSTR(&SRout); 

					CString Recstring; 
					Recstring.Empty(); 
					Recstring = SRout; 
					//这里开始处理语音识别的消息。
					// MessageBox(Recstring);
					speech(L"刚刚你说了" + Recstring);
				} 
				break; 
			} 
		}
	}
	return NULL;
}