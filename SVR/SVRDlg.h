
// SVRDlg.h : 头文件
//

#pragma once
#include <sapi.h> 
#include <sphelper.h>			//语音识别头文件

// CSVRDlg 对话框
class CSVRDlg : public CDialog
{
// 构造
public:
	CSVRDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_SVR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedButV2t();
	afx_msg void OnBnClickedButT2v();
	afx_msg void OnBnClickedButInit();
	afx_msg LRESULT OnRecord(WPARAM wParam,LPARAM lParam);

	//定义变量
	CComPtr<ISpRecognizer>	m_pSREngine;// 语音识别引擎(recognition)的接口。
	CComPtr<ISpRecoContext>	m_pSRContext;// 识别引擎上下文(context)的接口。
	CComPtr<ISpRecoGrammar>	m_pSRGrammar;// 识别文法(grammar)的接口。
	CComPtr<ISpStream>		m_pInputStream;// 流()的接口。
	CComPtr<ISpObjectToken>	m_pToken;// 语音特征的(token)接口。
	CComPtr<ISpAudio>		m_pAudio;// 音频(Audio)的接口。(用来保存原来默认的输入流)
	ULONGLONG ullGrammerID ;

	

	void processV();

	/************************************************************************/
	/* 语音合成                                                               */
	/************************************************************************/
	void speech(LPCTSTR str);


};

const int WM_RECORD = WM_USER + 100;//定义消息